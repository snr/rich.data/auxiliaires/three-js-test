# test Blender / ThreeJS

## présentation

test d'intégration d'un fichier `gltf` produit dans Blender sur ThreeJS.
l'objectif est de vérifier que le nom des objets Blender peut être exporté
en `gltf`, et récupérer sur ThreeJS. cela permettra de faire la connexion entre
le modèle 3D et la base de données: on définit comme nom pour chaque objet un 
identifiant de lieu, et on utilise ces identifiants dans des requêtes asynchrones 
pour récupérer les données liées à ce lieu dans ThreeJS.

le modèle est très moche, mais il contient 3 objets, chacun avec pour `name` un
identifiant de lieu. son intégration est aussi très brute de décoffrage.

## utilisation


```bash
git clone https://gitlab.inha.fr/snr/rich.data/auxiliaires/three-js-test.git
cd three-js-test
npx serve .
# se connecter à `localhost:3000` et clicker sur les objets
```

## licence

gnu gpl 3.0
