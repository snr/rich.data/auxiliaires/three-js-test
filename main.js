import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import WebGL from 'three/addons/capabilities/WebGL.js';
import * as THREE from "three";
import "jquery";

$.ready.then(() => {
  if ( WebGL.isWebGLAvailable() ) {
    console.log("WebGL is available !");

    // basic variables
    let loader
        , scene
        , camera
        , pointer
        , renderer
        , raycaster
        , ambientLight
        , directionalLight
        , picked;        // variable holding the selected object. will be defined in `initGltf.selectObject()`
    const onColor = 0xff00;   // selected object will be colored in green

    // execution logic
    initBase();
    initGltf();

    function initBase() {
      /**
       * basic initialization of the viewer
       */
      // define the variables
      scene = new THREE.Scene();
      loader = new GLTFLoader();
      renderer = new THREE.WebGLRenderer();
      camera = new THREE.PerspectiveCamera(75                                      // `fov`: field of view: extent of the scene on display, or angle of the frustum, in degrees
                                           , window.innerWidth/window.innerHeight  // `aspect`: ratio (width/height) of the camera
                                           , 0.1, 100);                            // `near`/`far` object further than `far` or closer than `near` aren't rendered
      pointer = new THREE.Vector2();
      raycaster = new THREE.Raycaster();
      ambientLight = new THREE.AmbientLight(0x404040);
      directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);

      // add settings
      camera.position.set(-3, 5, 10)
      renderer.setSize(window.innerWidth, window.innerHeight);  // width and height of the rendered (the size it will take on screen)
      scene.add(directionalLight);
      scene.add(ambientLight);

      // add the viewer to the dom
      document.body.appendChild(renderer.domElement);
    }

    function initGltf() {
      /**
       * load the Gltf viewer and define its logic
       */
      loader.load(
        "https://digital.inha.fr/richelieu/code/threejs_test/test.glb"
        , (gltf) => {
          // gltf is loaded
          console.log("gltf loaded");
          console.log("gltf", gltf)
          console.log("gltf scale", gltf.scene.scale);

          // gltf logic
          scene.add(gltf.scene);
          defineEventListeners();
          animate();

          // extra functions
          function objectPicker(e) {
            /**
             * select an object on click, color it and add a modal with its name
             *
             * ************
             * NEEDS FIXING: when clicking on one object, it selects all of them....
             * ************
             *
             * raycaster+pointer adapted from     : https://threejs.org/docs/#api/en/core/Raycaster
             * adapted from picking documentation : https://threejs.org/manual/#en/picking
             * see also this example              : https://github.com/mrdoob/three.js/blob/master/examples/webgl_interactive_cubes.html
             */
            // 1) TRANSLATE THE POINTER POSITION IN 3D WORLD COORDINATES. range -1..1
            pointer.x = ( e.clientX/window.innerWidth ) * 2 - 1;
            pointer.y = -( e.clientY/window.innerHeight ) * 2 + 1;

            // 2) SELECT THE CLICKED OBJECT AND PROCESS IT
            // restore the state of the previous `picked` + remove the modal
            if ( picked ) {
              picked.material.emissive.setHex( picked.backupHex );
              picked = null;
              $(".picked-title-container > .picked-title").remove();
            }

            // use `raycaster` to get intersections
            raycaster.setFromCamera(pointer, camera);
            let intersectionTgt;          // the objects which can be intersected: scene.children also contains lights => keep only the container of objects
            scene.children.map((c) => {
              if ( c.name === "Scene" ) {
                intersectionTgt = c.children;
              }
            })
            const intersects = raycaster.intersectObjects(intersectionTgt, false);  // false => non-recursive search

            // objects have been intersected => set `picked`,
            // save the unselected color, color `picked`,
            // add an HTML modal
            if (intersects.length) {

              picked = intersects[0].object;
              picked.backupHex = picked.material.emissive.getHex();  // `backupHex` is a custom property to store the color of an unselected object
              picked.material.emissive.setHex(onColor);

              console.log(picked.name);
              $(".picked-title-container").append($("<span></span>").addClass("picked-title")
                                                                    .text(picked.name));
            }
          }

          function animate() {
            /**
             * this function renders, or animates the scene. without this function,
             * the scene isn't shown
             */
            requestAnimationFrame(animate);  // refreshing time is based on the monitor's fps

            // rotate the object
            gltf.scene.rotation.x += 0.01;
            gltf.scene.rotation.y += 0.01;

            renderer.render(scene, camera);
          }

          function defineEventListeners() {
            /**
             * all our event listeners are defined in a function for more clarity
             */
            $("body").on('click', objectPicker);
          }

        }
        , undefined
        , (err) => {
          console.log("error");
          console.log(err);
        }
      )
    }

  } else {
    console.log("WebGL is not available !");
  }
})


// unused code blocs
/* if a camera is included, center to the camera
const gltfCamPos = gltf.cameras[0].position;
camera.position.set(gltfCamPos.x, gltfCamPos.y, gltfCamPos.z);  // define camera position from the gltf camera
*/

/* center the geometries
gltf.scene.traverse((child) => {
child.isMesh ? child.geometry.center() : false;
})
*/
